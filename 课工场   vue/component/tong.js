let tong = Vue.component('tong', {
    template: `
      <div class="tpxiao" style="background-color: #eee;">
          <ul class="tong-box" v-for="item in tong">
             <div class="tong-zi">{{item.zi}}</div>
            <li v-for="item1 in item.aaa">
                <div class="tong-left">
                    <img :src="'img/'+item1.pic"/>
                </div>
                <div class="tp-right">
		            <div class="name">
		              {{item1.name}}
					</div>
				</div>
            </li>
        </ul>
        </div>
        `,data(){
        return{
            tong:[]
        }
    },
    created(){
        axios.get("data/tong.json").then((res)=>{
            this.tong=res.data
        })
    }
})
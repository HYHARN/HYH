Vue.component("simba",{
    template:`
    <div>
    <i v-for=" i in count" class="fa star " :class="i<=aa? 'fa-star':'fa-star-o'" @mouseover="aa=i" @mouseout="aa=score"
       @click="score=i"></i>
</div>
    `,
    props:["c","value"],
    data(){
        return {
            count:5,
            score:0,
            aa:0
        }
    },
    watch:{
      score(val){
          this.$emit('input',val)
        }
    },
    mounted(){
       if(this.c&&!isNaN(this.c)){
           let c=parseInt(this.c)
           if(c>0){
               this.count=c
           }
       }
        if(this.value&&!isNaN(this.value)){
            let value=parseInt(this.value)
            if(value>0){
                this.score=value
            }
        }
        if(this.score>this.count)this.score=this.count
        this.aa=this.score
    }
})
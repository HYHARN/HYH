Vue.component("hand", {
    template: `
    <div style="width: 400px;height: 200px;margin: 0 auto;">
        <h1 v-html="str" style="width: 400px;height: 150px;color: red;font-size: 20px;"></h1>

        <div style="float: right"><button @click="fu()">{{arr[index].da[0]}}</button><button @click="fn()">{{arr[index].da[1]}}</button></div>
    </div>
    `,
    props: {
        arr: {
            type: Array,
            required: true
        }
    },
    data() {
        return {
            str:"",
            tempstr:``,
            index:0
        }
    },
    methods: {
        load(callback) {
            var i = 0;
            var timer = setInterval(() => {
                if (i === this.tempstr.length - 1) {
                    clearInterval(timer);
                    if (callback) callback();
                    return false;
                }

                this.str += this.tempstr[i++];
            }, 100);
        },
        fu() {
            this.tempstr = "<br/>" + this.arr[this.index].da1[0];
            this.load(this.next);
        },
        fn() {
            this.tempstr = "<br/>" + this.arr[this.index].da1[1];
            this.load(this.next);
        },
        next() {
            this.str = '';
            if (++this.index === this.arr.length) {
                this.index = 0;
            }
            this.tempstr = this.arr[this.index].wen;
            this.load()

        }
    },
    mounted() {
        this.tempstr = this.arr[this.index].wen
        this.load()
        console.log(this.arr)
    }

})
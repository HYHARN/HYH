let fa = Vue.component('fa', {
    template: `
      <div class="tpxiao" style="background-color: #eee;">
          <ul class="fa-box" v-for="item in fa1">
            <li v-for="item1 in item.ss">
                <div class="fa-left">
                    <img :src="'img/'+item1.pic"/>
                </div>
                <div class="tp-right">
		            <div class="name">
		              {{item1.name}}
					</div>
				</div>
            </li>
        </ul>
        </div>
        `,data(){
        return{
            fa1:[]
        }
    },
    created(){
        axios.get("data/fa.json").then((res)=>{
            this.fa1=res.data
        })
    }
})